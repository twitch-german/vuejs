let arrayDeProductosDesdeJS = [
    {
        nombre: "Lagtcham",
        foto: "lagtcham.jpg",
        descripcion: "Viewer interactivo, capaz de tirar 500 palabras por minuto en el chat",
        precio: 1000
    },
    {
        nombre: "Daltomico",
        descripcion: "Es el tomi... y bello, metti, además está certificado por cierta plataforma que no puedo mencionar tampoco...",
        foto: "daltomico.jpg",
        precio: 10000000
    },
    {
        nombre: "Fenavente",
        descripcion: "El barba más copado del universo linuxero, que después de ayudarme con Vue JS me va a ayudar con NODE y MONGO",
        foto: "fenavente.jpg",
        precio: 0
    },
    {
        nombre: "JunglaRetro",
        foto: "junglaretro.webp",
        descripcion: "Coder de la vieja escuela (retro), directamente del xhtml/css2 a sus hogares",
        precio: 50000
    }
];