/*
const Home = {
    template: '<home-joaquin  :listadoProductos=listadoProductos />',
    name: "Pamsho_"
}
const Carrito = { template: '<div>Carrito</div>' }

const router = new VueRouter({
    routes: [
        { path: '/', component: Home },
        { path: '/carrito', component: Carrito },
    ],
});
*/
const app = new Vue( {
    el: "#Faculpx15",
    //router,
    data: {
        totalComprado: 0,
        modalAbierto: false,
        modalData: { },
        miclase: 'red',
        siteName: "MercadoRancio.com",
        listadoProductos: arrayDeProductosDesdeJS
    },
    mounted( ){
        window.addEventListener('keydown', this.teclita );   
    },
    methods: {
        mostrar( producto ){
            this.modalAbierto = true;
            this.modalData = producto;
        },
        ocultar( ){
            this.modalAbierto = false;
        },
        teclita( e ){
            if(e.key == 'Escape'){
                this.modalAbierto = false;
            }
        }
    }
} );
